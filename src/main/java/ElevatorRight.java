package main.java;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static main.java.ElevatorRight.Direction.*;


/**
 * Created by Anna Galkina on 25-12-2017.
 */
public class ElevatorRight implements Runnable {
    private static int floorOnWhereElev;
    private static boolean isDoorOpened;

    private static TreeSet<Integer> floorsGoingToAsc = new TreeSet<>();
    private static TreeSet<Integer> floorsGoingToDesc = (TreeSet<Integer>) floorsGoingToAsc.descendingSet();
    private static Set<Integer> floorsPushedInsideElevator = new TreeSet<>();
    private static List<ButtonOnFloor> buttonOnFloors = new ArrayList<>();

    private static Direction direction;
    private static Direction previousState = UP;

    public boolean isDoorOpened() {
        return isDoorOpened;
    }

    public void setIsDoorOpened(boolean isDoorOpened) {
        ElevatorRight.isDoorOpened = isDoorOpened;
    }

    public int getFloorOnWhereElev() {
        return floorOnWhereElev;
    }

    public void setFloorOnWhereElev(int floorOnWhereElev) {
        ElevatorRight.floorOnWhereElev = floorOnWhereElev;
    }

    private Direction getDirection() {
        return direction;
    }

    private void setDirection(Direction direction) {
        ElevatorRight.direction = direction;
    }

    public void pushButtonInsideElevator(int floorButton) {
        floorsPushedInsideElevator.add(floorButton);
        floorsGoingToAsc.add(floorButton);
    }

    public void call(int currentFloor, ButtonOnFloor.ButtonDirection buttonDirection) {
        buttonOnFloors.add(new ButtonOnFloor(currentFloor, buttonDirection));
        floorsGoingToAsc.add(currentFloor);
        this.notify();
    }

    public void openDoors() throws InterruptedException {
        setDirection(STAY);
        setIsDoorOpened(true);
        Thread.sleep(3000);
        setIsDoorOpened(false);
        setDirection(previousState);
    }

    private void goElevatorRight() throws InterruptedException {
        while (true) {
            Floor.buttonOnFloors.forEach((floor, buttons) -> {
                if (!(buttons.isButtonInsideElevatorOn() || buttons.isButtonOnFloorDownOn() || buttons.isButtonOnFloorUpOn())) {
                    setDirection(STAY);
                }
            });

            if (getDirection().equals(UP)) floorOnWhereElev++;
            else floorOnWhereElev--;
            System.out.println("2 --------- Elevator is on floor " + floorOnWhereElev + " going to " + floorsGoingToAsc + ". State is " + getDirection().name());

            Button buttonsOnFloor = Floor.buttonOnFloors.get(floorOnWhereElev);

            if (buttonsOnFloor.isButtonInsideElevatorOn() || getDirection().equals(UP) && buttonsOnFloor.isButtonOnFloorUpOn() || getDirection().equals(DOWN) && buttonsOnFloor.isButtonOnFloorDownOn()) {
                openDoors();
            }

            if (buttonsOnFloor.isButtonInsideElevatorOn()) {
                buttonsOnFloor.setButtonInsideElevatorOn(false);
            }

            if (getDirection().equals(UP) && buttonsOnFloor.isButtonOnFloorUpOn()) {
                buttonsOnFloor.setButtonOnFloorUpOn(false);
            }

            if (getDirection().equals(DOWN) && buttonsOnFloor.isButtonOnFloorDownOn()) {
                buttonsOnFloor.setButtonOnFloorDownOn(false);
            }


            //if there are either button floor is in the same direction as elevator or floor buttons inside elevator are higher than elevator position when elevator is going up
            if (!buttonOnFloors.stream().filter(b -> getPredicate(b.getFloor())  //if button is above/below current position of elevator
                    && isSameDirectionButtonAndElevator(b.getDirectionOnButton(), previousState)) // if button direction is same as elevator direction
                    .collect(Collectors.toList()).isEmpty()
                    || !floorsPushedInsideElevator.stream().filter(this::getPredicate).collect(Collectors.toList()).isEmpty()) {
                setDirection(previousState);
            } else {
                if (previousState.equals(UP))
                    setDirection(DOWN);
                else setDirection(UP);
            }
            System.out.println("3 --------- Elevator is on floor " + floorOnWhereElev + " going to " + floorsGoingToAsc + ". State is " + getDirection().name());
        }
    }

    private boolean getPredicate(int buttonFloor) {
        return previousState.equals(UP) ? buttonFloor > floorOnWhereElev : buttonFloor < floorOnWhereElev;
    }

    private boolean isSameDirectionButtonAndElevator(ButtonOnFloor.ButtonDirection b, ElevatorRight.Direction e) {
        return b.mod == e.mod;
    }

    private void setInitialState() throws InterruptedException {
        setDirection(STAY);
        setFloorOnWhereElev(1);
        this.wait();

        if (floorsGoingToAsc.first() > floorOnWhereElev) {  //direction is defined based on first pushed value
            setDirection(UP);
        } else if (floorsGoingToAsc.first() == floorOnWhereElev) {
            openDoors();
        } else {
            setDirection(DOWN);
        }
    }

    @Override
    public void run() {
        try {
            setInitialState();
            goElevatorRight();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    enum Direction {
        UP(1),
        DOWN(-1),
        STAY(0);

        int mod;

        Direction(int mod) {
            this.mod = mod;
        }
    }
}
