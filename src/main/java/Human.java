package main.java;

import java.util.Random;

import static main.java.House.FLOORS_IN_BUILDING;

public class Human implements Runnable {
    private int humanOnFloor;
    private int floorToPush;
    private ButtonOnFloor.ButtonDirection buttonDirection;
    private Elevator elevator;

    public Human(int humanOnFloor) {
        this.humanOnFloor = humanOnFloor;
        populateRandomDecisions();
    }

    public int getHumanOnFloor() {
        return humanOnFloor;
    }

    public void setHumanOnFloor(int humanOnFloor) {
        this.humanOnFloor = humanOnFloor;
    }

    public int getFloorToPush() {
        return floorToPush;
    }

    public void setFloorToPush(int floorToPush) {
        this.floorToPush = floorToPush;
    }

    public ButtonOnFloor.ButtonDirection getButtonDirection() {
        return buttonDirection;
    }

    public void setButtonDirection(ButtonOnFloor.ButtonDirection buttonDirection) {
        this.buttonDirection = buttonDirection;
    }

    private void populateRandomDecisions() {
        setButtonDirection(getRandomDirection());
        setFloorToPush(new Random().nextInt(FLOORS_IN_BUILDING));
    }


    public Human walksToElevator() {
        System.out.println(Thread.currentThread().getName() + " generated on floor " + getHumanOnFloor() + " and  walks to elevator");
        elevator = new Elevator();
        return this;
    }

    public Human callsElevator() {
        System.out.println(Thread.currentThread().getName() + " calls elevator in direction " + getButtonDirection().name());
        elevator.call(getHumanOnFloor(), getButtonDirection());
        return this;
    }

    public synchronized Human waitsDoorsOpened() throws InterruptedException {
        System.out.println(Thread.currentThread().getName() + " waits until door's opened");
        while (!(elevator.isDoorOpened() && elevator.getFloorOnWhereElev() == getHumanOnFloor())) {
            //TODO wait
        }
        return this;
    }

    public Human goesInsideElevator() {
        return this;
    }

    public Human pushesButtonInsideElevator() {
        elevator.pushButtonInsideElevator(getFloorToPush());
        return this;
    }

    public synchronized Human waitsUntilHeIsOnFloorHePressedInsideElevator() throws InterruptedException {
        while (!(elevator.isDoorOpened() && elevator.getFloorOnWhereElev() == getFloorToPush())) {
            this.wait();
        }
        return this;
    }

    public Human goesOut() {
        return this;
    }

    @Override
    public void run() {
        try {
            walksToElevator()
                    .callsElevator()
                    .waitsDoorsOpened()
                    .goesInsideElevator()
                    .pushesButtonInsideElevator()
                    .waitsUntilHeIsOnFloorHePressedInsideElevator()
                    .goesOut();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private ButtonOnFloor.ButtonDirection getRandomDirection() {
        ButtonOnFloor.ButtonDirection[] values = ButtonOnFloor.ButtonDirection.values();
        return values[new Random().nextInt(values.length)];
    }
}
