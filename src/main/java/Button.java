package main.java;

/**
 * Created by Anna Galkina on 25-12-2017.
 */
public class Button {
    private boolean buttonOnFloorUpOn;
    private boolean buttonOnFloorDownOn;
    private boolean buttonInsideElevatorOn;

    public Button(boolean buttonOnFloorUpOn, boolean buttonOnFloorDownOn, boolean buttonInsideElevatorOn) {
        this.buttonOnFloorUpOn = buttonOnFloorUpOn;
        this.buttonOnFloorDownOn = buttonOnFloorDownOn;
        this.buttonInsideElevatorOn = buttonInsideElevatorOn;
    }

    public boolean isButtonOnFloorUpOn() {
        return buttonOnFloorUpOn;
    }

    public void setButtonOnFloorUpOn(boolean buttonOnFloorUpOn) {
        this.buttonOnFloorUpOn = buttonOnFloorUpOn;
    }

    public boolean isButtonOnFloorDownOn() {
        return buttonOnFloorDownOn;
    }

    public void setButtonOnFloorDownOn(boolean buttonOnFloorDownOn) {
        this.buttonOnFloorDownOn = buttonOnFloorDownOn;
    }

    public boolean isButtonInsideElevatorOn() {
        return buttonInsideElevatorOn;
    }

    public void setButtonInsideElevatorOn(boolean buttonInsideElevatorOn) {
        this.buttonInsideElevatorOn = buttonInsideElevatorOn;
    }


}
