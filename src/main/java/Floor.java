package main.java;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anna Galkina on 25-12-2017.
 */
public class Floor {
    public static Map<Integer, Button> buttonOnFloors = new HashMap<>();
    public static int floor;

    static {
        for (int i = 1; i <= House.FLOORS_IN_BUILDING; i++) {
            buttonOnFloors.put(i, new Button(false, false, false));
        }
    }
}
