package main.java;

import java.util.Random;

import static main.java.House.FLOORS_IN_BUILDING;

/**
 * Created by Anna Galkina on 25-12-2017.
 */
public class MainClazz {

    public static void main(String[] args) {
        Elevator elevator = new Elevator();
        Thread elevatorThread = new Thread(elevator, "Elevator");
        elevatorThread.start();

        for (int i = 1; i < 2; i++) {
            Thread human = new Thread(new Human(new Random().nextInt(FLOORS_IN_BUILDING)), "Human #" + i);
            human.start();
        }
    }
}
