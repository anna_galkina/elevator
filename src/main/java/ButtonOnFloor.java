package main.java;

/**
 * Created by Anna Galkina on 28-12-2017.
 */
public class ButtonOnFloor {

    private int floor;

    private ButtonDirection directionsOnButton;

    public ButtonOnFloor(int floor, ButtonDirection directionsOnButton) {
        this.floor = floor;
        this.directionsOnButton = directionsOnButton;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public ButtonDirection getDirectionOnButton() {
        return directionsOnButton;
    }

    public void setDirectionsOnButton(ButtonDirection directionsOnButton) {
        this.directionsOnButton = directionsOnButton;
    }

    public enum ButtonDirection {
        UP(1),
        DOWN(-1);

        int mod;

        ButtonDirection(int mod) {
            this.mod = mod;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ButtonOnFloor that = (ButtonOnFloor) o;

        if (floor != that.floor) return false;
        return directionsOnButton == that.directionsOnButton;

    }

    @Override
    public int hashCode() {
        int result = floor;
        result = 31 * result + (directionsOnButton != null ? directionsOnButton.hashCode() : 0);
        return result;
    }
}
